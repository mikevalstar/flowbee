import ConfigContext from '../lib/ConfigContext/ConfigContext';
import ConfigElementPicker from '../lib/ConfigElementPicker/ConfigElementPicker';
import ConfigElementPreview from '../lib/ConfigElementPreview/ConfigElementPreview';
import ConfigElementProperties from '../lib/ConfigElementProperties/ConfigElementProperties';

import './Example.scss';

/*
 * Note: this file is only for dev, nothing in here will be included into the library build
   https://codesandbox.io/s/github/react-dnd/react-dnd/tree/gh-pages/examples_ts/03-nesting/drag-sources?from-embed=&file=/src/TargetBox.tsx:99-185
 */

function Example () {
  return <ConfigContext>
    <div className='example'>
      <div className='example-c1'>
        <ConfigElementPicker />
      </div>
      <div className='example-c2'>
        <ConfigElementPreview />
      </div>
      <div className='example-c3'>
        <ConfigElementProperties />
      </div>
    </div>
  </ConfigContext>;
}

export default Example;
