export enum eType {
  INPUT = 'input',
  DROPDOWN = 'dropdown',
}

export enum eMode {
  PICKER = 'picker',
  PREVIEW = 'preview',
  SETTINGS = 'settings',
}
