import { useState, useId, useContext } from 'react';
import { useDroppable, useDndMonitor, DragEndEvent } from '@dnd-kit/core';
import { useAutoAnimate } from '@formkit/auto-animate/react';
import { nanoid } from 'nanoid';

import ConfigContext from '../ConfigContext/ConfigContext';
import { eMode } from '../const';

import FormElement from '../FormElements/FormElements';
import { ReactComponent as PlusSVG } from '../../assets/plus.svg';

function arrayMove (arr: Array<any>, oldIdx: number, newIdx: number) {
  if (newIdx >= arr.length) {
    let k = newIdx - arr.length + 1;
    while (k--) {
      arr.push(undefined);
    }
  }
  arr.splice(newIdx, 0, arr.splice(oldIdx, 1)[0]);
  return arr; // for testing
}

function ConfigElementPreview () {
  const { formConfig, updateField, removeField, reorderFields, currentlyEditing, setCurrentlyEditing } = useContext(ConfigContext.ctx);
  const [parent] = useAutoAnimate<HTMLDivElement>(/* optional config */);
  const [fields, setFields] = useState([] as Array<any>);
  const dropId = useId();

  const { setNodeRef } = useDroppable({
    id: dropId,
  });

  const addField = (event: DragEndEvent) => {
    const type = event.active.data?.current?.type;
    const mode = event.active.data?.current?.mode;
    const dropPos = event.active.rect.current.translated || { top: 0, bottom: 0 };
    const dropItemId = event.active.id;
    const movingUp = event.delta.y <= 0;

    let after = movingUp ? 0 : -1; // on the way down it counts itself
    let bestFit = 0;
    for (const row of parent?.current?.children || []) {
      const getRect = row.getBoundingClientRect();

      if (movingUp && getRect.bottom < dropPos.top && bestFit < getRect.bottom) {
        after++;
        bestFit = getRect.bottom;
      } else if (!movingUp && getRect.top < dropPos.bottom && bestFit < getRect.bottom) {
        after++;
        bestFit = getRect.bottom;
      }
    }

    if (mode === eMode.PREVIEW) {
      setFields((prevState) => {
        const curIdx = prevState.findIndex(f => f.id === dropItemId);
        const newOrder = [...arrayMove(prevState, curIdx, Math.min(after, prevState.length - 1))];

        reorderFields(newOrder.map(m => m.id));

        return newOrder;
      });
    } else {
      const newId = nanoid(10);
      updateField(newId, { type, id: newId, config: false });

      setFields((prevState) => {
        return [...prevState, { type, id: newId }];
      });
    }
  };

  useDndMonitor({
    onDragEnd (event) {
      if (event?.over?.id === dropId) {
        addField(event);
      }
    },
  });

  return <div className='tf-base tf-config-preview'>
    <h2>Form preview</h2>
    <div ref={setNodeRef}>
      <h3>Your new form</h3>
      <div ref={parent}>
        {fields.map(m => <div key={m.id}>
          <FormElement id={m.id} type={m.type} config={formConfig.fields.find((f:any) => f.id === m.id)}
            onConfigClick={() => {
              setCurrentlyEditing(m.id);
            }}
            onDeleteClick={() => {
              removeField(m.id);
              setFields((prevState) => {
                const items = [...prevState];

                const fIdx = items.findIndex(f => f.id === m.id);

                if (fIdx >= 0) {
                  items.splice(fIdx, 1);
                }

                return items;
              });
            }}
            mode={eMode.PREVIEW} />
        </div>)}
        {fields.length <= 0 && <div className="tf-preview-plus">
          <PlusSVG />
        </div>}
      </div>
      <button onClick={() => { console.log(formConfig); }}>
        Console the Config
      </button>
    </div>
  </div>;
}

export default ConfigElementPreview;
