import TextInput from './TextInput/TextInput';
import Dropdown from './Dropdown/Dropdown';
import { eType } from '../const';

import ElementInterface from './ElementInterface';

interface ElementInterfaceSelector extends ElementInterface {
  type: eType;
}

function FormElement ({ type, ...otherProps }: ElementInterfaceSelector) {
  switch (type) {
  case eType.INPUT:
    return <TextInput {...otherProps} />;
  case eType.DROPDOWN:
    return <Dropdown {...otherProps} />;
  }

  return <div>Error</div>;
}

export default FormElement;
