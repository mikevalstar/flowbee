import { useContext, useState, useId } from 'react';
import ConfigContext from '../../ConfigContext/ConfigContext';
import ElementInterface from '../ElementInterface';
import { eType, eMode } from '../../const';
import Picker from '../Picker';
import { useDraggable } from '@dnd-kit/core';
import { CSS } from '@dnd-kit/utilities';
import { ReactComponent as DropdownSVG } from '../../../assets/list-dropdown.svg';
import { ReactComponent as TrashSVG } from '../../../assets/trash.svg';
import { ReactComponent as ConfigSVG } from '../../../assets/wrench.svg';
import { ReactComponent as HandleSVG } from '../../../assets/grip-vertical.svg';

export function DropdownPicker ({ mode }: ElementInterface) {
  const { attributes, listeners, setNodeRef, transform, isDragging } = useDraggable({
    id: useId(),
    data: {
      mode,
      type: eType.DROPDOWN,
    },
  });

  const style = {
    position: isDragging ? 'relative' : '',
    zIndex: 20,
    transform: CSS.Translate.toString(transform),
  } as React.CSSProperties;

  return <div className={'tf-draggable'} style={style} ref={setNodeRef} {...attributes} {...listeners}>
    <Picker text="Dropdown" icon={<DropdownSVG />}/>
  </div>;
}

export function DropdownPreview ({ id, mode, config, onConfigClick, onDeleteClick }: ElementInterface) {
  const { attributes, listeners, setNodeRef, transform, isDragging } = useDraggable({
    id: id || useId(),
    data: {
      mode,
      type: eType.DROPDOWN,
    },
  });

  const style = {
    position: isDragging ? 'relative' : '',
    zIndex: 20,
    transform: CSS.Translate.toString(transform),
  } as React.CSSProperties;

  return <div className='tf-preview-item' style={style} ref={setNodeRef} {...attributes}>
    <div className='tf-preview-item-header'>
      <h3>Dropdown {id}</h3>
      <span><a onClick={onDeleteClick}><TrashSVG /></a></span>
      <span><a onClick={onConfigClick}><ConfigSVG /></a></span>
      <span className='tf-draggable' {...listeners} ><HandleSVG /></span>
    </div>
    <div className='tf-preview-item-content'>
      <label>
        <span>{config.name || 'no name'}</span>
        <select>
          <option>Example option 1</option>
        </select>
      </label>
    </div>
  </div>;
}

export function TextInputSettings ({ config }: ElementInterface) {
  const { updateField } = useContext(ConfigContext.ctx);

  const [name, setName] = useState(config.name);

  return <>
    <h3>Settings</h3>
    Name: <input value={name} onChange={e => setName(e.target.value)}/>
    <button onClick={() => {
      const newDeets = { ...config, name };
      updateField(config.id, newDeets);
    }}>Save</button>
  </>;
}

function Dropdown (props: ElementInterface) {
  switch (props.mode) {
  case eMode.PICKER:
    return <DropdownPicker {...props} />;
  case eMode.PREVIEW:
    return <DropdownPreview {...props} />;
  case eMode.SETTINGS:
    return <div>Settings here</div>;
  }
  return <div>Error</div>;
}

export default Dropdown;
