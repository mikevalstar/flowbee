import { eMode } from '../const';

export default interface ElementInterface {
  mode: eMode;
  id: string | null;
  onConfigClick?: () => void;
  onDeleteClick?: () => void;
  config?: any;
  className?: string;
};
