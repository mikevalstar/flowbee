import { ReactElement } from 'react';
import { ReactComponent as GripSVG } from '../../assets/grip-vertical.svg';

interface ElementInterfaceSelector {
  text: string;
  icon: ReactElement;
}

function Picker ({ text, icon }: ElementInterfaceSelector) {
  return <div className='tf-fe-picker'>
    <div className='tf-icon'>{icon}</div>
    <div className='tf-text'>{text}</div>
    <div className='tf-handle'><GripSVG/></div>
  </div>;
}

export default Picker;
