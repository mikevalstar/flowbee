import { useContext } from 'react';
import ConfigContext from '../ConfigContext/ConfigContext';
import { eMode } from '../const';

import FormElement from '../FormElements/FormElements';

function Example () {
  const { currentlyEditing } = useContext(ConfigContext.ctx);

  return <div className='tf-base'>
    <h2>Element Properties Here</h2>
    CE: {currentlyEditing?.id}

    {currentlyEditing
      ? <FormElement
        id={currentlyEditing.id}
        type={currentlyEditing.type}
        config={currentlyEditing}
        mode={eMode.SETTINGS} />
      : <></>}
  </div>;
}

export default Example;
