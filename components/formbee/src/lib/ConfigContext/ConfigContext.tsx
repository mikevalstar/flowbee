import React, { useState } from 'react';

import { DndContext } from '@dnd-kit/core';

const ConfigContextBase = React.createContext({
  formConfig: null as any,
  currentlyEditing: null as any | null,
  setCurrentlyEditing: (id: string) => {},
  updateField: (id: string, value: any) => {},
  reorderFields: (ids: Array<string>) => {},
  removeField: (id: string) => {},
});
ConfigContextBase.displayName = 'ForrmbeeConfigContext';

interface ConfigContextType {
  children: React.ReactNode;
  value?: {
    title: string;
    fields: Array<any>;
  }
}

const ConfigContext = ({ children, value } : ConfigContextType) => {
  const [currentlyEditing, setCurrentlyEditingData] = useState(null);
  const [formConfig, setFormConfig] = useState(value || {
    title: '',
    fields: [] as Array<any>,
  });

  const updateField = (id: string, value: any) => {
    setFormConfig((prevState) => {
      const newForm = { ...prevState };

      const fIdx = newForm.fields.findIndex(f => f.id === id);

      if (fIdx >= 0) {
        newForm.fields[fIdx] = value;
      } else {
        newForm.fields.push(value);
      }

      return newForm;
    });
  };

  const removeField = (id: string) => {
    setFormConfig((prevState) => {
      const newForm = { ...prevState };

      const fIdx = newForm.fields.findIndex(f => f.id === id);

      if (fIdx >= 0) {
        newForm.fields.splice(fIdx, 1);
      }

      return newForm;
    });
  };

  const reorderFields = (ids: Array<string>) => {
    setFormConfig((prevState) => {
      const newForm = { ...prevState };

      newForm.fields = ids.map(m => {
        return newForm.fields.find(f => f.id === m);
      });

      return newForm;
    });
  };

  const setCurrentlyEditing = (id: string) => {
    setCurrentlyEditingData({ ...formConfig.fields.find(f => f.id === id) });
  };

  return <DndContext>
    <ConfigContextBase.Provider value={{
      formConfig,
      currentlyEditing,
      setCurrentlyEditing,
      updateField,
      reorderFields,
      removeField,
    }}>
      {children}
    </ConfigContextBase.Provider>
  </DndContext>;
};

ConfigContext.ctx = ConfigContextBase;

export default ConfigContext;
