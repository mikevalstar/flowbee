import { useMemo } from 'react';
import FormElement from '../FormElements/FormElements';
import { eMode, eType } from '../const';

function ConfigElementPicker () {
  const elements = useMemo(() => Object.values(eType), []);

  return <div className='tf-base tf-config-picker'>
    <h2>Add new form element</h2>

    {elements.map(m => {
      return <FormElement id={null} key={m} type={m} mode={eMode.PICKER}/>;
    })}
  </div>;
}

export default ConfigElementPicker;
