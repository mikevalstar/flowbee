import Example from './Example/Example';

import './App.scss';

/*
 * Note: this file is only for dev, nothing in here will be included into the library build
 */

function App () {
  return <div className="App">
    <Example />
  </div>;
}

export default App;
