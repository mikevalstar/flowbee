import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

/*
 * Note: this file is only for dev, nothing in here will be included into the library build
 * This is just to render it on the dev site
 */

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
);
